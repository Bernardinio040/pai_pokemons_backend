const express = require('express');
var cors = require('cors');
const app = express();
const port = 8000;

app.use(cors());

const router = require("./routes/basicRouter");

app.use('/pokedex', router);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})