const express = require('express');
const router = express.Router();
const fs = require("fs");
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));
let pokedex = require("../assets/pokedex.json");

router.get('/', (req, res) => {
    const sort = req.query.sort;
    const name = req.query.name;
    let type = req.query.type;
    //JSON.parse

    let result = pokedex;
    if (name !== undefined) {
        result = pokedex.filter(pokemon => pokemon.name.english.toLowerCase().startsWith(name.toLowerCase()));    
    }

    if (sort !== undefined) {
        switch(sort) {
            case "nameasc":
                result.sort((a,b) => {
                    return a.name.english.localeCompare(b.name.english);
                })
                break;

            case "namedesc":
                result.sort((a,b) => {
                    return b.name.english.localeCompare(a.name.english);
                })
                break;

            case "idasc":
                result.sort((a,b) => {
                    return a.id > b.id ? 1 : a.id < b.id ? -1 : 0;
                })
                break;

            case "iddesc":
                result.sort((a,b) => {
                    return a.id < b.id ? 1 : a.id > b.id ? -1 : 0;
                })
                break;
        }
    }

    if (type !== undefined) {
        type = req.query.type;
        //Array.isArray(req.query.type) ? req.query.type : [req.query.type]
        const newTypes = type.map((t) => {
            return t.toLowerCase();
        });

        result = result.filter((pokemon) => {
            const pokemonTypes = pokemon.type.map((t) => {
                return t.toLowerCase();
            });

            let pokemonFlag = false;
            newTypes.forEach((t) => {
                pokemonFlag |= pokemonTypes.includes(t);
            });

            return pokemonFlag;
        });
    }
    
    res.json(result);
})

router.delete('/delete/:id', function(req, res) {
    let pokemon_id = req.params.id;

    pokedex = pokedex.filter(pokemon => pokemon.id != pokemon_id);

    fs.writeFileSync(__dirname + "/../assets/pokedex.json", JSON.stringify(pokedex));

    res.json(pokedex);
});

router.get('/new', function(req, res) {
    fetch("https://raw.githubusercontent.com/fanzeyi/pokemon.json/master/pokedex.json").then(resp => resp.json().then(o =>{
        fs.writeFileSync(__dirname + "/../assets/pokedex.json", JSON.stringify(o))
        res.json(o);
    }));
})

module.exports = router;