import React from 'react';
import axios from 'axios';
import "./App.css";
import types from './types.json';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params : { type : [] },
      pokemons: []
    }
  }

  componentDidMount() {
    this.generator(this.state.params);
  }

  tableGenerator = (array) => {
    let out = array.data.map((pokemon, i) => {
      let path = "./assets/" + pokemon.id.toString().padStart(3, '0') + "MS.png";
      return(<tr>
        <td>{++i}</td>
        <td>{ pokemon.name.english }</td>
        <td><img src={require(`${path}`)}></img></td>
        <td><button onClick={ () => this.delete(pokemon.id) }>Delete</button></td>
      </tr>);
    })
    this.setState({pokemons: out});
  }

  generator = (params) => {
    console.log(params);
    axios.get('http://localhost:8000/pokedex', {
      params : params
    })
    .then((res) => {
      this.tableGenerator(res);
    })
  }

  delete = (id) => {
    axios.delete('http://localhost:8000/pokedex/delete/' + id) 
    .then((res) => {
      this.tableGenerator(res);
    })
  }

  restore = () => {
    axios.get('http://localhost:8000/pokedex/new/')
    .then((res) => {
      this.tableGenerator(res);
    })
  }

  searchFunction = (text) => {
    const params = Object.assign(this.state.params);
    params.name = text;
    this.setState(params);
    this.generator(params);
  }

  sortingFunction = (direction) => {
    // let params = {};
    // params[sortType] = direction;
    const params = Object.assign(this.state.params);
    params.sort = direction;
    this.setState(params);
    this.generator(params);
  }

  filteringFunction = (typeB) => {
    const params = Object.assign(this.state.params);
    if (this.state.params.type.includes(typeB)) {
      let table = this.state.params.type;
      let index = table.indexOf(typeB);
      table.splice(index, 1);
      params.type = table;
      this.setState(params);
    } else {
      params.type = [...this.state.params.type, typeB];
      this.setState(params);
    }
    this.generator(params);
  }

  render() {
    let colors = ["#ccc9aa", "#e81319", "#5eb9b2", "#a819d7", "#e1d158", "#776a3e", "#bddd6e", "#8e55a4", "#7b8e8a", "#f67f0b", "#0a7abc", "#3e9709", "#fffa24", "#ec0e63", "#1995a1", "#8a55fd", "#5f4632", "#ffa0c2"];
    let i = 0;
    console.log(this.state.params);
    return (
      <div className="mainBox">
        <div className="searchBarBox">
          <div className="searchBar">
            <h2>Search by name: </h2><input type="text" onChange={(x) => {this.searchFunction(x.target.value)}}></input>
          </div>
        </div>
        <div className="sortingButtonsBox">
          <button onClick={ () => { this.sortingFunction("nameasc") } } style={{ marginRight: "8px" }}> Sort by name asc </button>
          <button onClick={ () => { this.sortingFunction("namedesc") } } style={{ marginRight: "8px" }}> Sort by name desc </button>
          <button onClick={ () => { this.sortingFunction("idasc") } } style={{ marginRight: "8px" }}> Sort by id asc </button>
          <button onClick={ () => { this.sortingFunction("iddesc") } } style={{ marginRight: "8px" }}> Sort by id desc </button>
        </div>
        <div className="filterByTypeBox">
          {
              types.map((type)=>{
                return <button onClick={ () => { this.filteringFunction(type.english) } } style={{ backgroundColor: colors[i++], marginRight: "6px", color: type.english == "Electric" ? "black" : "white" }}>{ type.english }</button>;
              })
          }
        </div>
        <button className="restore" onClick={ this.restore }>Restore pokemons</button>
        <div className="table">
          <table>
            <thead>
              <tr>
                <th>Lp.</th>
                <th>Name</th>
                <th>Picture</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {this.state.pokemons}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
